﻿
using Couchbase.Linq;
using fake_api.data;

namespace fake_api.repository
{
    public class CouchRepository : IRepository
    {
        private BucketContext bucketContext;

        public CouchRepository(BucketContext bucketContext)
        {

            this.bucketContext = bucketContext;
        }

        public  Task<IEnumerable<BaseItem>> Find(int appId, int countryId, string platfrom)
        {
               
          var result =   bucketContext.Query<CampaignCouch>()
             .Where(x =>
            x.countries.Contains(countryId) &&
            x.platforms.Contains(platfrom) &&
            x.campaignConfigurations.Exists(a => a.appId == appId)
                 ).ToList();

            return Task.FromResult<IEnumerable<BaseItem>>(result);
        }
    }
}
