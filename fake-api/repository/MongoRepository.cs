﻿using fake_api.Context;
using fake_api.data;
using MongoDB.Driver;

namespace fake_api.repository
{
    public class MongoRepository : IRepository
    {
        private MongoContext context; 

        public MongoRepository(MongoContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<BaseItem>> Find(int appId, int countryId, string platfrom)
        {

            /*
            
             */
            FilterDefinition<CampaignMongo> filter = Builders<CampaignMongo>
                 .Filter
                 .Where(x =>
                 x.countries.Contains(countryId) &&
                 x.platforms.Contains(platfrom) &&
                 x.campaignConfigurations.Any(c => c.appId == appId)
                 );

            return await context.campaigns
                   .Find(filter)
                   .ToListAsync();
        }

        public async Task<bool> Decrease(CampaignMongo campaignMongo)
        {
            var updatedResult = await context
                                       .campaigns
                                       .ReplaceOneAsync(filter: x => x.campaignId == campaignMongo.campaignId, replacement: campaignMongo);

            return updatedResult.IsAcknowledged
             && updatedResult.ModifiedCount > 0;

        }

    }
}
