﻿using fake_api.data;

namespace fake_api.repository
{
    public interface IRepository
    {

        public Task<IEnumerable<BaseItem>> Find(int appId, int countryId, string platfrom);
    }
}
