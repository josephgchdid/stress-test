﻿using fake_api.data;
using fake_api.repository;
using Microsoft.AspNetCore.Mvc;

namespace fake_api.controller
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DataController : ControllerBase
    {
        private MongoRepository mongoRepository;

        public DataController(MongoRepository mongoRepository)
        {
            this.mongoRepository = mongoRepository;
        }

        [HttpGet]
        [Route("mongo")]
        public async Task<ActionResult<IEnumerable<CampaignMongo>>> findMongo(int appId, int countryId, string platform)
        {
            var result =  await mongoRepository.Find(appId, countryId, platform);

            foreach(CampaignMongo campaign in result)
            {
                campaign.campaignConfigurations.ForEach(x => x.impressionsLeft--);

                await mongoRepository.Decrease(campaign);
            }
            return Ok(result);
        }
         

    }
}
