﻿using fake_api.data;
using fake_api.repository;
using Microsoft.AspNetCore.Mvc;

namespace fake_api.controller
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CouchController : ControllerBase
    {
        
        private CouchRepository couchRepository;

        public CouchController(CouchRepository couchRepository)
        {
            this.couchRepository = couchRepository;
        }

        [HttpGet]
        [Route("couch")]
        public async Task<ActionResult<IEnumerable<CampaignCouch>>> findMongo(int appId, int countryId, string platfrom)
        {
            var result = await couchRepository.Find(appId, countryId, platfrom);

            return Ok(result);
        }

    }
}
