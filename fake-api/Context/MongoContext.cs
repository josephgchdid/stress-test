﻿using fake_api.data;
using fake_api.seed;
using MongoDB.Driver;

namespace fake_api.Context
{
    public class MongoContext
    {
        public IMongoCollection<CampaignMongo> campaigns { get;  }

        public MongoContext(IConfiguration configuration)
        {
            var client = new MongoClient(
                configuration.GetValue<string>("DataBaseSettings:ConnectionString")
            );

            var database = client.GetDatabase(
                configuration.GetValue<string>("DataBaseSettings:DatabaseName")
            );

            campaigns = database.GetCollection<CampaignMongo>(
             configuration.GetValue<string>("DataBaseSettings:CollectionName")
             );

            bool seedExists = campaigns.Find(p => true).Any();

            if(!seedExists)
            {
                campaigns.InsertMany(MongoSeed.Seed());
            }
        }
    }
}
