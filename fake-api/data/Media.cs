﻿namespace fake_api.data
{
    public class Media
    {
        public Dictionary<string, CampaignContent> content { get; set; }
        public string thumbnail { get; set; }
        public string mediaPath { get; set; }
    }
}
