﻿using MongoDB.Bson.Serialization.Attributes;

namespace fake_api.data
{

    public class CampaignMongo : BaseItem
    {
        [BsonId]
        public string campaignId { get; set; }

        [BsonElement("campaignName")]
        public string campaignName { get; set; }

        [BsonElement("contentType")]
        public string contentType { get; set; }

        [BsonElement("platforms")]
        public List<string> platforms { get; set; }

        [BsonElement("advertiserId")]
        public int advertiserId { get; set; }

        [BsonElement("media")]
        public List<Media> media { get; set; }

        [BsonElement("countries")]
        public List<int> countries { get; set; }

        [BsonElement("campaignConfigurations")]
        public List<CampaignConfiguration> campaignConfigurations { get; set; }
    }


}
