﻿namespace fake_api.data
{
    public class CampaignContent
    {
        public string title { get; set; }
        public string description { get; set; }
        public string buttonTitle { get; set; }
        public string url { get; set; }
    }

}
