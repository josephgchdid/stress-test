﻿namespace fake_api.data
{
    public class CampaignCouch : BaseItem
    {
        public string campaignId { get; set; }
         
        public string campaignName { get; set; }

        public string contentType { get; set; }

        public List<string> platforms { get; set; }

        public int advertiserId { get; set; }

        public List<Media> media { get; set; }

        public List<int> countries { get; set; }

        public List<CampaignConfiguration> campaignConfigurations { get; set; }
    }
}
