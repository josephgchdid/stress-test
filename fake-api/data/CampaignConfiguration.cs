﻿namespace fake_api.data
{
    public class CampaignConfiguration
    {
        public int appId { get; set; }
        public int budget { get; set; }
        public int adCap { get; set; }
        public int impressionsLeft { get; set; }
        public bool isActive { get; set; }
        public int priority { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public bool show_advertiser_name { get; set; }
        public bool show_bubble_on_android { get; set; }
        public string channel { get; set; }
        
    }
}
