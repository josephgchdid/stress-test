﻿using Couchbase.Extensions.DependencyInjection;
using Couchbase.Linq;
using fake_api.Context;
using fake_api.repository;

namespace fake_api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
          
            services.AddControllers();


            /*
            services.AddCouchbase(Configuration.GetSection("Couchbase"))
                .AddCouchbaseBucket<ICampaignBucketProvider>("campaign");

            services.AddTransient(async x =>
            {
                var campaignBucket = x.GetRequiredService<ICampaignBucketProvider>();
                return new BucketContext(await campaignBucket.GetBucketAsync());
            });



            services.AddScoped<CouchRepository>();
              */
            services.AddScoped<MongoContext>();

            services.AddScoped<MongoRepository>();


        }

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime appLifetime)
        {
   
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            appLifetime.ApplicationStopped.Register(() =>
            {
                app.ApplicationServices.GetRequiredService<ICouchbaseLifetimeService>().Close();
            });
        }

    }
}
