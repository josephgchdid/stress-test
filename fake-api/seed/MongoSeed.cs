﻿using fake_api.data;

namespace fake_api.seed
{
    public class MongoSeed
    {
        public static IEnumerable<CampaignMongo> Seed()
        {
            var content = new Dictionary<string, CampaignContent>
            {
                ["english"] = new CampaignContent()
                {
                    title = "title",
                    description = "description",
                    buttonTitle = "buttonTitle",
                    url = "https://google.com"
                }
            };


            var apps = new List<CampaignConfiguration>()
            {

                new CampaignConfiguration()
                {
                    appId = 1,
                    budget = 100,
                    adCap = 1,
                    impressionsLeft = 1000000000,
                    isActive = true,
                    priority = 1,
                    createdAt = "createdAt",
                    updatedAt = "updatedAt",
                    startDate = "startDate",
                    endDate = "endDate",
                    show_advertiser_name = true,
                    show_bubble_on_android = true,
                    channel = "channel"
                },
                 new CampaignConfiguration()
                {
                    appId = 2,
                    budget = 100,
                    adCap = 1,
                    impressionsLeft = 1000000000,
                    isActive = true,
                    priority = 1,
                    createdAt = "createdAt",
                    updatedAt = "updatedAt",
                    startDate = "startDate",
                    endDate = "endDate",
                    show_advertiser_name = true,
                    show_bubble_on_android = true,
                    channel = "channel"
                },
                  new CampaignConfiguration()
                {
                    appId = 3,
                    budget = 100,
                    adCap = 1,
                    impressionsLeft = 1000000000,
                    isActive = true,
                    priority = 1,
                    createdAt = "createdAt",
                    updatedAt = "updatedAt",
                    startDate = "startDate",
                    endDate = "endDate",
                    show_advertiser_name = true,
                    show_bubble_on_android = true,
                    channel = "channel"
                }
            };


            return new List<CampaignMongo>()
            {
                new CampaignMongo()
                {
                    campaignName = "campaign stress test",
                    campaignId = "11",
                    contentType = "video",
                    platforms = new List<string> { "ALL", "ANDROID"},
                    advertiserId = 1,
                    media = new List<Media>()
                    {
                        new Media()
                        {
                            content = content,
                            mediaPath = "mediaPath",
                            thumbnail = "thumbnail"

                        }
                    },

                    countries = new List<int>() { 1,2,3,4,5,6,7,8,9,10},
                    campaignConfigurations = apps
                }
            };
        }
    }
}
